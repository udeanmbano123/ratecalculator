<?php
/*
Author-Udean Mbano
Purpose- Api for rate calculator
*/
$data =file_get_contents('php://input');

$a = json_decode($data, true);
$sourceAmount=$a["sourceAmount"];
$sourceCountry=$a["sourceCountry"];
$destinationCountry=$a["destinationCountry"];
$sourceCurrency=$a["sourceCurrency"];
$destinationCurrency=$a["destinationCurrency"];
$rate=$a["rate"];
$charges=$a["fees_charge_promo_applied_rate"];
$destinationAmount=$sourceAmount*$rate;
$totaltopay=$destinationAmount+($charges*$rate);
$payment_method=$a["payment_method"];
$service_level=$a["service_level"];
$transfer_type=$a["transfer_type"];
$promo_code=$a["promo_code"];
$data = '{
	"sourceCountry":"'.$sourceCountry.'",
    "sourceCurrency":"'.$sourceCurrency.'",
    "sourceAmount": '.$sourceAmount.',
	"destinationCountry":"'.$destinationCountry.'",
	"destinationCurrency":"'.$destinationCurrency.'",
    "destinationAmount":'.$destinationAmount.',
	"rate":'.$rate.',
	"fees_charge_promo_applied_rate":'.$fees_charge_promo_applied_rate.',
	"payment_method":"'.$payment_method.'",
    "service_level":"'.$service_level.'",
	"transfer_type":"'.$transfer_type.'",
    "promo_code":"'.$promo_code.'",
	"totaltopay":'.$totaltopay.',
}';
header('Content-type:application/json;charset=utf-8');
echo $data;
?>
<!--
Author-Udean Mbano
Purpose- Display rate calculator
-->
<!doctype html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<title>Rate Calculator-Udean Mbano</title>
<link rel="stylesheet" href="https://test.remit.by/accessfinancetest/corporate-calculator/css/bootstrap.min.css" type="text/css" />
	<link rel="stylesheet" href="https://test.remit.by/accessfinancetest/corporate-calculator/css/bootstrap-select.min.css" type="text/css" />
	<link rel="stylesheet" href="https://test.remit.by/accessfinancetest/corporate-calculator/css/calculator.css" type="text/css" />
	
	<script type="text/javascript" src="https://test.remit.by/accessfinancetest/corporate-calculator/js/jquery.min.js"></script>
	<script type="text/javascript" src="https://test.remit.by/accessfinancetest/corporate-calculator/js/jquery-ui.min.js"></script>
	<script type="text/javascript" src="https://test.remit.by/accessfinancetest/corporate-calculator/js/bootstrap.min.js"></script>
	<script type="text/javascript" src="https://test.remit.by/accessfinancetest/corporate-calculator/js/bootstrap-select.min.js"></script>
	<script type="text/javascript" src="https://test.remit.by/accessfinancetest/corporate-calculator/js/currencyFormatter.min.js"></script>
	<script type="text/javascript" src="https://test.remit.by/accessfinancetest/corporate-calculator/js/bind-with-delay.js"></script>
	<script type="text/javascript" src="https://test.remit.by/accessfinancetest/corporate-calculator/js/calculator.js"></script>
	<!--[if lt IE 9]>
	<script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
	<![endif]-->
	<style>
		#container {
			width: 800px;
			margin: 0 auto;
			padding-top: 20px;
		}
		
		#remitone-calculator form > div {
			margin: 0;
		}
		
		#remitone-calculator .source_amount {
			border-bottom-left-radius: 0;
			border-bottom: 0;
		}
		#remitone-calculator .rate {
			font-weight:bold !important;
		}
		#remitone-calculator .source_amount + div button {
			border-bottom-right-radius: 0;
			border-bottom: 0;
		}
		#remitone-calculator .destination_amount {
			border-top-left-radius: 0;
		}
		#remitone-calculator .destination_amount + div button {
			border-top-right-radius: 0;
		}
		
		#remitone-calculator .source_country, #remitone-calculator .destination_country,#remitone-calculator .source_amount, #remitone-calculator .destination_amount,#remitone-calculator .payment_method,#remitone-calculator .service_level,#remitone-calculator .transfer_type,#remitone-calculator .promo_code {
			font-size: 2em;
			height: 72px;
			padding-top: 20px;
			padding-bottom: 10px;
		}
		
		#remitone-calculator .source_amount_currency_section, #remitone-calculator .destination_amount_currency_section {
			position: relative;
		}
		#remitone-calculator label {
			position: absolute;
			top: 5px;
			left: 30px;
			z-index: 99;
			color: #0e243e;
			font-size: 0.9em;
		}
		
		#remitone-calculator > div {
			margin: 0;
		}
		#remitone-calculator .fees_charge_promo_applied_rate_section, #remitone-calculator .error_message_section {
			margin: 5px 0;
			font-weight:bold !important;
		}
		
		#remitone-calculator .input-group-btn.calculator_currency_dropdown, #remitone-calculator .source_amount + div button, #remitone-calculator .destination_amount + div button {
			color: #fff;
			font-size: 1.2em;
		}
		#remitone-calculator .source_amount + div button, #remitone-calculator .destination_amount + div button {
			background-color: #0e243e;
		}
		#remitone-calculator .source_amount + div button:hover, #remitone-calculator .destination_amount + div button:hover:hover {
			background-color: #000B25;
		}
		#remitone-calculator .btn .caret {
			margin-left: 10px;
		}
		#remitone-calculator .calculator_currency_dropdown .dropdown-menu, #remitone-calculator .calculator_currency_dropdown .btn-default {
			min-width: 100px !important;
		}
		
		#remitone-calculator .source_amount_currency_section button.btn {
			padding-top: 20px;
			padding-bottom: 23px;
		}
		#remitone-calculator .destination_amount_currency_section button.btn {
			padding-top: 20px;
			padding-bottom: 22px;
		}
	</style>
</head>
<body>
	<div id="container">
		<div id="remitone-calculator"></div>
	</div>

	<script type="text/javascript">
		var remitone = $("#remitone-calculator").calculator({
			base_url: 'https://test.remit.by/accessfinancetest/',
			orientation: 'horizontal',
		    submit_url: 'https://udeanmbano.co.zw/rate.php',	
			order: [
			    'source_country',
            	'source_amount_currency',
				'destination_country',
				'destination_amount_currency',
			    'payment_method',
                'service_level',
				'transfer_type',
                'promo_code',
                'fees_charge_promo_applied_rate',
              	'error_message',
				'send_money',
			],
			
			source_amount_currency: {
				render: null,
				label: 'From Amount',
				defaultAmountValue: 1000,
				defaultCurrencyValue: null,
			},
			payment_method: {
				render: null,
				label: 'Payment Method',
			},
			service_level: {
				render: null,
				label: 'Service Level',
			},
			destination_amount_currency: {
				render: null,
				label: 'To Amount',
				defaultAmountValue: null,
				defaultCurrencyValue: null,
			},
			rate: {
				render: null,
				label: 'Today`s Exchange Rate',
				},
				// Store raw data
	send_money: {
				render: null,		// null|$('#custom')
				text:'Transfer Now',			// null|'Custom Text'
			},
		
		});
	
		
	</script>
	
</body>
</html>